package com.huntersmeadow.wordpuzzleassist.workclasses


import android.annotation.SuppressLint
import android.content.DialogInterface
import android.view.View
import com.huntersmeadow.wordpuzzleassist.WORDPUZZLEASSISTApp


/** Generates an empty click listener.
 *
 *  @return A blank click listener.
 */
fun getEmptyClickListener(): DialogInterface.OnClickListener
{
    return DialogInterface.OnClickListener { _, _ -> }
}


/** Blank view object to pass as a dummy object. */
@SuppressLint("StaticFieldLeak")
val unusedView = View(WORDPUZZLEASSISTApp.appContext())


object PublicConstants
{
    const val THREAD_PROGRESS_MAX = 100
    const val FPS60 = 1000/60L
}