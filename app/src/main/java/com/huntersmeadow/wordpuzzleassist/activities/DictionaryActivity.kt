package com.huntersmeadow.wordpuzzleassist.activities


import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.huntersmeadow.wordpuzzleassist.R
import com.huntersmeadow.wordpuzzleassist.workclasses.ThreadManager


/** Dictionary activity. */
class DictionaryActivity : AssistBaseActivity()
{


    //-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
    //OVERRIDES
    //-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dictionary)
        setActionBarTitle(getString(R.string.activity_title_dictionary))
        setIDs(R.id.dictionary_search,
               R.id.dummyStopButton,
               R.id.dictionary_input,
               R.id.dictionary_results,
               -1,
               ::startDictionary)
    }


    override fun helpMenuCALLBACK()
    {
        AlertDialog.Builder(this)
            .setMessage(getString(R.string.dictionary_help_text))
            .setTitle(getString(R.string.help_title))
            .create()
            .show()
    }


    override fun resetDefaults()
    {
        // Empty.
    }


    //-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
    //CALLBACKS
    //-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~


    // Redundancy suppression because layout wants THIS activity to have this function.
    @Suppress("Redundant")
    override fun startAssistCALLBACK(view: View)
    {
        super.startAssistCALLBACK(view)
    }


    // Redundancy suppression because layout wants THIS activity to have this function.
    @Suppress("Redundant")
    override fun stopAssistCALLBACK(view: View)
    {
        super.stopAssistCALLBACK(view)
    }


    // Redundancy suppression because layout wants THIS activity to have this function.
    @Suppress("Redundant")
    override fun clearInputCALLBACK(view: View)
    {
        super.clearInputCALLBACK(view)
    }


    //-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~
    //PRIVATE
    //-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~-~


    /** Method unique to this activity that is passed to the parent as a generic 'start assist
     *  thread' method.
     *
     *  @param input String input to start the assist thread with.
     */
    private fun startDictionary(input:String)
    {
        registerThreadWithSelf(ThreadManager.instance().startDictionary(input))
    }


}